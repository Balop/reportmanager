<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\checkreport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/reports', 'HomeController@reports')->name('reports')->middleware(['verified','checkreport']);

Route::get('/edit_user', 'HomeController@edit_user')->name('edit_user')->middleware('verified');

Route::get('/new_employee', 'HomeController@new_employee')->name('new_employee')->middleware(['verified','checkreport']);

Route::post('/xyy', 'HomeController@save')->name('save')->middleware('verified');

Route::post('/editdetails', 'HomeController@editdetails')->name('editdetails')->middleware('verified');

Route::post('/checkedit', 'HomeController@checkedit')->name('checkedit')->middleware('verified');


Route::post('/xy', 'HomeController@save_report')->name('save_report')->middleware('verified');

Route::post('/deleteclient/{id}', 'HomeController@deleteclient')->name('deleteclient')->middleware('verified');

Route::get('/client_list', 'HomeController@client_list')->name('client_list')->middleware('verified');

Route::get('/report', 'HomeController@report')->name('report')->middleware('verified');

Route::post('/updateclient/{id}', 'HomeController@updateclient')->name('updateclient')->middleware('verified');

Auth::routes(['verify' => true]);

