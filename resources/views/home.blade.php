@extends('layouts.pagemaster')

@section('content')
<div class="app-content content container-fluid">
@if (\Session::has('success'))
<br>
    <div class="alert alert-success col-md-4"  style="float:right" id="success_btn" >
                    <p>{!! \Session::get('success') !!}</p>
                <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button>
            </div>
        @endif

      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <!-- stats -->
<div class="row">
    <div class="col-xl-4 col-lg-4 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="pink">278</h3>
                            <span>Completed Projects</span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-bag2 pink font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="teal">{{ $employeecount }}</h3>
                            <span>Total System Users</span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="deep-orange">64.89 %</h3>
                            <span>Project Completed Rate</span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ stats -->

                    <div class="row">
                        <div class="card-header">
                            <strong class="card-title">Recent Report</strong>
                        </div><br>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Work Accomplished</th>
                        <th>Date</th>
                        <th>View Description</th>

                      </tr>
                    </thead>
                    <tbody>

                         @if(Auth::user()->user_role == 1)

                         @foreach($reports as $key=> $report)
                      <tr>
                        <td>{{ $report->first_name }} {{$report->last_name}}</td>
                        <td>{{ $report->work_accomplished }}</td>
                        <td>{{ $report->created_at }}</td>
                         <td class="text-center">
                                    <button class="btn-primary btn-sm" data-toggle="modal" data-id="{{ $report->id }}" style="margin-left: 40%" data-target="#editModal{{$report->id}}"><i class="icon-info"></i></button>
                                </td>
                      </tr>

                      <div id="editModal{{$report->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Details Of Work Done By {{$report->first_name}} {{$report->last_name}}</h4><br>
                </div>
                <div class="modal-body">
                            <div class="modal-body">
                            
                                <div class="form-body">
                                 <p><strong>Description: </strong>{{ $report->description }}</p>
                                 <p><strong>Progress Made On Job:</strong> {{ $report->progress }}</p>
                                 <p><strong>Challenges faced at work today:</strong> {{ $report->challenge }}</p>
                                <div class="row">
                                    <div class="col-md-6">
                                       
                            </div>
                                <div class="modal-footer">
                                <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- ./add edit modal -->
                      @endforeach

                      @else

                       @foreach($persons as $key=> $person)
                      <tr>
                        <td>{{ $person->first_name }} {{$person->last_name}}</td>
                        <td>{{ $person->work_accomplished }}</td>
                        <td>{{ $person->created_at }}</td>
                        <td class="text-center">
                                    <button class="btn-primary btn-sm" data-toggle="modal" data-id="{{ $person->id }}" style="margin-left: 40%" data-target="#editModal{{$person->id}}"><i class="icon-info"></i></button>
                                </td>
                      </tr>


                      <div id="editModal{{$person->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Details Of Work Done By {{$person->first_name}} {{$person->last_name}}</h4><br>
                </div>
                <div class="modal-body">
                            <div class="modal-body">
                            
                                <div class="form-body">
                           <p><strong>Description: </strong>{{ $person->description }}</p>
                                 <p><strong>Progress Made On Job:</strong> {{ $person->progress }}</p>
                                 <p><strong>Challenges faced at work today:</strong> {{ $person->challenge }}</p>
       
                                   <div class="row">
                                    <div class="col-md-6">
                        
                            </div>
                                <div class="modal-footer">
                                <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- ./add edit modal -->
                      @endforeach

                      @endif


                       <div class="modal fade text-xs-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <label class="modal-title text-text-bold-600" id="myModalLabel33">Inline Login Form</label>
                        </div>
                        <form action="#">
                            <div class="modal-body">
                          <label>Email: </label>
                          <div class="form-group">
                            <input type="text" placeholder="Email Address" class="form-control">
                          </div>

                          <label>Password: </label>
                          <div class="form-group">
                            <input type="password" placeholder="Password" class="form-control">
                          </div>
                          </div>
                          <div class="modal-footer">
                          <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                          <input type="submit" class="btn btn-outline-primary btn-lg" value="Login">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>

                      
                    </tbody>
                  </table>
                </div>
            </div>
                    
@endsection
