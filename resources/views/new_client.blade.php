@extends('layouts.pagemaster')

@section('content')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
          </div>
          @if (\Session::has('success'))
    <div class="alert alert-success col-md-4"  style="float:right" id="success_btn" >
                    <p>{!! \Session::get('success') !!}</p>
                <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button>
            </div>
        @endif
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Enroll Employee</a>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Basic form layout section start -->
<section id="basic-form-layouts">
    <div class="row match-height">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title" id="basic-layout-form">Employee Info</h2>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="card-text" style="margin-left: 70%">
                         <form class="form" method="POST" action="{{ route('save') }}">
                            @csrf


                            <h4 class="form-section"><i class="icon-head form-control">&nbsp;Job Title</i>
                            <select name="job_spec" class="form-control @error('job_spec') is-invalid @enderror">
                                <option value="0" selected="" disabled="disabled">-- Select Job Spec --</option>
                                @foreach($jobs as $key=> $job)
                                <option value="{{$job->job_spec}}">{{$job->job_spec}}</option>
                                @endforeach
                            </select></h4>
                             
                             @error('job_spec')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-head"></i> Login Credentials</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" id="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" placeholder="Username" name="username">

                                             @error('username')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                    

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">E-email</label>
                                            <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-mail">

                                             @error('email')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <label for="password">Password</label>
                                           <i class="icon-eye" onclick="action()"></i>
                                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">

                                             @error('password')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <i class="icon-eye" onclick="action2()"></i>
                                            <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="Confirm Passowrd">
                                        </div>
                                    </div>
                                </div><hr><br>
                               
                            <h4 class="form-section"><i class="icon-info"></i> Additional Information</h4>
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fname">First Name</label>
                                            <input type="text" id="fname" class="form-control @error('fname') is-invalid @enderror" name="fname" value="{{ old('fname') }}" placeholder="First Name">

                                             @error('fname')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lname">Last Name</label>
                                            <input type="text" id="lname" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" placeholder="Last Name">

                                             @error('lname')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">phone</label>
                                            <input type="number" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}">

                                            @error('phone')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="gender">Gender</label>
                                            <select id="gender" name="gender" class="form-control @error('gender') is-invalid @enderror">
                                                 <option value="0" selected="" disabled="disabled">-- Select gender --</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>

                                             @error('gender')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div><hr>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary mr-1">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
<script type="text/javascript">
            function action() {
    var my_pass = document.getElementById("password");
    if (my_pass.type === "password") {
      my_pass.type = "text";
    } else {
      my_pass.type = "password";
    }
  }

        function action2() {
    var my_pass = document.getElementById("password_confirmation");
    if (my_pass.type === "password") {
      my_pass.type = "text";
    } else {
      my_pass.type = "password";
    }
  }

</script>