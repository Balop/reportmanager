@extends('layouts.pagemaster')

@section('content')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-xs-12 mb-1">
          </div>
          @if (\Session::has('success'))
    <div class="alert alert-success col-md-4"  style="float:right" id="success_btn" >
                    <p>{!! \Session::get('success') !!}</p>
                <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button>
            </div>
        @endif
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Reports</a>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Basic form layout section start -->
<section id="basic-form-layouts">
    <div class="row match-height">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title" id="basic-layout-form">Daily Work Report</h2>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                       
                         <form class="form" method="POST" action="{{ route('save_report') }}">
                            @csrf
                            <div class="form-body">
                                <h4 class="form-section"><i class="icon-head"></i> Work Detail</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="job">Accomplished Job</label>
                                            <input type="text" id="job" class="form-control @error('job') is-invalid @enderror" value="{{ old('job') }}" placeholder="Job Title" name="job">

                                             @error('job')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                    

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="progress">Work progress</label>
                                            <input type="text" id="progress" class="form-control @error('progress') is-invalid @enderror" name="progress" value="{{ old('progress') }}" placeholder="Progress on work course">

                                             @error('progress')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div><hr><br>
                               
                            <h4 class="form-section"><i class="icon-info"></i> Detailed Description</h4>
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="description">Work Description</label>
                                            <textarea type="text" cols="4" rows="5" id="description" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" placeholder="Give full account of the day's job"></textarea>

                                             @error('description')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="challenge">Challenge(s)</label>
                                            <textarea type="text" cols="4" rows="5" id="challenge" class="form-control @error('challenge') is-invalid @enderror" name="challenge" value="{{ old('challenge') }}" placeholder="(Optional)"></textarea>

                                             @error('challenge')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    </div>
                                </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary mr-1">
                                    <i class="icon-check2"></i> Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
<script type="text/javascript">
            function action() {
    var my_pass = document.getElementById("password");
    if (my_pass.type === "password") {
      my_pass.type = "text";
    } else {
      my_pass.type = "password";
    }
  }

        function action2() {
    var my_pass = document.getElementById("password_confirmation");
    if (my_pass.type === "password") {
      my_pass.type = "text";
    } else {
      my_pass.type = "password";
    }
  }

</script>