@extends('layouts.pagemaster')

@section('content')

<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- stats -->
<div class="row">
</div>
<!--/ stats -->

<!-- Recent invoice with Statistics -->
<div class="row match-height">

   @if (\Session::has('deleted'))
    <div class="alert alert-success col-md-4"  style="float:right" id="delete_btn" >
                    <p>{!! \Session::get('deleted') !!}</p>
                <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('delete_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button>
            </div>
        @endif

         @if (\Session::has('success'))
    <div class="alert alert-success col-md-4"  style="float:right" id="success_btn" >
                    <p>{!! \Session::get('success') !!}</p>
                <button type="submit" class="btn btn-primary mr-1" onclick="document.getElementById('success_btn').style.display = 'none'">
                    <i class="icon-check2"></i> Close</button>
            </div>
        @endif
        
    <div class="col-xl-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">All Time Clients</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
                <div class="table-responsive">
                    <table class="table table-hover mb-0 ">
                        <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Date</th>

                                @if(Auth::user()->user_role == 1)
                                <th class="text-center" colspan="2">Action</th>
                                @endif

                            </tr>
                        </thead>
                        <tbody>
                           @foreach($clients as $key=> $client)
                            <tr>
                                <td class="text-center">{{ $key + 1 }}</td>
                                <td class="text-center">{{ $client->first_name }} {{ $client->last_name }}</td>
                                <td class="text-center">{{ $client->email }}</span></td>
                                <td class="text-center">{{ $client->phone }}</td>
                                <td class="text-center">{{ $client->state }}</td>
                                <td class="text-center">{{ $client->created_at }}</td>
                                
                                 @if(Auth::user()->user_role == 1)
                                <td class="text-center">
                                    <button class="btn-primary btn-sm" data-toggle="modal" data-id="{{ $client->id }}" data-target="#editModal{{$client->id}}"><i class="icon-edit"></i></button>
                                </td>
                                <td class="text-center">
                                <form method="POST" action="{{route('deleteclient', $client->id)}}">
                                @csrf
                                   <button type="submit" class="btn-primary btn-sm"><i class="icon-trash"></i></button> 
                                </form>
                                </td>
                                @endif
                            </tr>
                            <!-- add edit modal -->
<div id="editModal{{$client->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Edit {{$client->first_name}} {{$client->last_name}}</h4><br>
                </div>
                <div class="modal-body">
                 <form method="POST" action="{{route('updateclient', $client->id)}}">
                     @csrf
                            <div class="modal-body">
                            
                                <div class="form-body">
                                <h4 class="form-section"><i class="icon-head"></i> Personal Info</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fname">First Name</label>
                                            <input type="text" id="fname" class="form-control" value="{{ $client->first_name }}" placeholder="First Name" name="fname">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <label for="lname">Last Name</label>
                                            <input type="text" id="lname" class="form-control" name="lname" value="{{ $client->last_name }}" placeholder="Last Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mail">E-mail</label>
                                            <input type="email" id="mail" class="form-control" name="mail" value="{{ $client->email }}" placeholder="E-mail">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Contact Number</label>
                                            <input type="number" id="phone" class="form-control" name="phone" value="{{ $client->phone }}" placeholder="Phone No.">
                                        </div>
                                    </div>
                                </div>

                                <h4 class="form-section"><i class="icon-location"></i> Address</h4>

                                <div class="form-group">
                                    <label for="address">Company Address</label>
                                    <textarea cols="10" rows="4" id="address" class="form-control" placeholder="Address" name="address">{{ $client->address }}</textarea> 
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select id="state" name="state" class="form-control">
                                                <option value="{{ $client->state }}">{{ $client->state }} state</option>
                                                <option value="lagos">Lagos State</option>
                                                <option value="niger">Niger State</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <select id="city" name="city" class="form-control">
                                                <option value="{{ $client->city }}">{{ $client->city }}</option>
                                                <option value="island">Lagos Island</option>
                                                <option value="mainland">Lagos Main Land</option>
                                            </select>
                                        </div>
                                    </div>

                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select id="country" name="country" class="form-control">
                                                 <option value="{{ $client->country }}">{{ $client->country }}</option>
                                                <option value="lagos">Nigeria</option>
                                                <option value="ogun">U.S.A</option>
                                                <option value="edo">Spain</option>
                                                <option value="niger">Brazil</option>
                                            </select>
                                        </div>
                                    </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="postal_code">Postal Code</label>
                                            <input type="text" id="postal_code" class="form-control" name="postal_code" value="{{ $client->postal_code }}" placeholder="E-mail">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="form-section"><i class="icon-info"></i> Additional Information</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook</label>
                                            <input type="text" id="facebook" class="form-control" name="facebook" value="{{ $client->facebook }}" placeholder="Facebook Name">
                                        </div>
                                    </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="instagram">Instagram</label>
                                            <input type="text" id="instagram" class="form-control" name="instagram" value="{{ $client->instagram }}" placeholder="Instagram Handle">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="twitter">Twitter</label>
                                            <input type="text" id="twitter" class="form-control" name="twitter" value="{{ $client->twitter }}" placeholder="Twitter Handle">

                                        </div>
                                    </div>

                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="website_url">Website Url</label>
                                            <input type="text" id="website_url" class="form-control" name="website_url" value="{{ $client->website_url }}" placeholder="website url">
                                        </div>
                                    </div>
                                </div>



                            </div>
                                <div class="modal-footer">
                                <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                <input type="submit" class="btn btn-outline-primary btn-lg" value="Update">
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
<!-- ./add edit modal -->
                            @endforeach

                        </tbody>
                    </table>

                    @if(isset($clients))
                     <div style="margin-left: 20px">
                        {{$clients->links()}}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
