@extends('layouts.pagemaster')

@section('content')
<div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

                    <div class="row">
                        <div class="card-header">
                            <strong class="card-title">Report Pull</strong>
                        </div><br>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Work Accomplished</th>
                        <th>Date</th> 
                        <th>View Description</th>

                      </tr>
                    </thead>
                    <tbody>

                         @foreach($reports as $key=> $report)
                      <tr>
                        <td>{{ $report->first_name }} {{$report->last_name}}</td>
                        <td>{{ $report->work_accomplished }}</td>
                        <td>{{ $report->created_at }}</td>
                         <td class="text-center">
                                    <button class="btn-primary btn-sm" data-toggle="modal" data-id="{{ $report->id }}" style="margin-left: 40%" data-target="#editModal{{$report->id}}"><i class="icon-info"></i></button>
                                </td>
                      </tr>

                      <div id="editModal{{$report->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Details Of Work Done By {{$report->first_name}} {{$report->last_name}}</h4><br>
                </div>
                <div class="modal-body">
                            <div class="modal-body">
                            
                    <div class="form-body">
                     <p><strong>Description: </strong>{{ $report->description }}</p>
                     <p><strong>Progress Made On Job:</strong> {{ $report->progress }}</p>
                     <p><strong>Challenges faced at work today:</strong> {{ $report->challenge }}</p>
                    <div class="row">
                        <div class="col-md-6">
                           
                </div>
                    <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                </div>
            </form>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!-- ./add edit modal -->
    @endforeach

          
        </tbody>
      </table>
    </div>
</div>
                    
@endsection
