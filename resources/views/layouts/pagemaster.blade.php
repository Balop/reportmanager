<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="User Account Page">
    <meta name="keywords" content="web app">
    <meta name="author" content="PIXINVENT">
    <title>TizyReporter</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('robust/app-assets/images/ico/apple-icon-60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('robust/app-assets/images/ico/apple-icon-76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('robust/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('robust/app-assets/images/ico/apple-icon-152.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('robust/app-assets/images/logo/tizy.png')}}">
   
  <link rel="shortcut icon" type="image/png" href="{{asset('robust/app-assets/images/logo/tizy.png') }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/bootstrap.css') }}">
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/fonts/icomoon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/vendors/css/extensions/pace.css') }}">
    <!-- END VENDOR CSS-->
    <!-- data table -->
        <link rel="stylesheet" href="{{asset('references/css/datatable/dataTables.bootstrap.min.css')}}">
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/colors.css') }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/css/core/colors/palette-gradient.css') }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('robust/app-assets/assets/css/style.css') }}">
    <!-- END Custom CSS-->

  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-light navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
            <li class="nav-item"><a href="{{ route('home') }}" class="navbar-brand nav-link"><img class="alt="branding logo" src="{{asset('robust/app-assets/images/logo/tizy.png') }}" style="width:70%; margin-bottom:20px" data-expand="{{asset('robust/app-assets/images/logo/tizy.png') }}" data-collapse="{{asset('robust/app-assets/images/logo/tizy.png') }}" class="brand-logo"></a></li>
            <!-- <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li> -->
            <li class="dropdown dropdown-user nav-item float-xs-right hidden-md-up"><a href="#" data-toggle="modal" data-target="#inlineForm" class=" nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{asset('robust/app-assets/images/portrait/small/avatar-s-1.png') }}" alt="avatar"><i></i></span><span class="user-name"><i class="icon-edit"></i></span></a>
              </li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid d-block">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle"><i class="icon-menu5"></i></a></li>

               <li class="dropdown dropdown-user nav-item float-sm-right"><a href="#" data-toggle="modal" data-target="#inlineForm" class="nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="{{asset('robust/app-assets/images/portrait/small/avatar-s-1.png') }}" alt="avatar"><i></i></span><span class="user-name"> Edit {{Auth::user()->username}} <i class="icon-edit"></i></span></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
      <!-- main menu header-->
     <!--  <div class="main-menu-header">
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class=" nav-item"><a href="{{ route('home') }}"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Dashboard</span></a></li>
          @if(Auth::user()->user_role == 1)
          <li class=" nav-item"><a href="{{ route('new_employee') }}"><i class="icon-stack-2"></i><span data-i18n="nav.page_layouts.main" class="menu-title">Enroll Employee</span></a>
          </li>
          @endif
           <li class=" nav-item"><a href=""><i class="icon-stack-2"></i><span data-i18n="nav.page_layouts.main" class="menu-title">Reports</span></a>
            <ul class="menu-content">
              <li><a href="{{ route('report') }}"  class="menu-item">Create Report</a>
              </li>
              @if(Auth::user()->user_role == 1)
              <li><a href="{{route('reports')}}"  class="menu-item">View Employee Reports</a>
              </li>
              @endif
            </ul>
          </li>
          <li class=" nav-item"><a href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                               <i class="icon-power3"></i><span data-i18n="nav.support_raise_support.main" class="menu-title">
                              {{ __('Logout') }}
                            </span>
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                          </form>
                          </li>
                    </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- include includes/menu-footer-->
      <!-- main menu footer-->
    </div>
    <!-- / main menu-->


    
     <main class="py-4 navbar-border">

        <div class="modal fade text-xs-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <label class="modal-title text-text-bold-600" id="myModalLabel33"><strong>Confirm Password</strong></label>
                        </div>
                        <form action="{{ route('checkedit') }}" method="post">
                          @csrf
                            <div class="modal-body">
                            <div class="form-group" >
                                            <label for="password">Password</label>
                                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="password" name="password" required="required" minlength="8">

                                             @error('password')
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        </div>
                                    <div>
                                      <input type="text" name="id" value="{{Auth::user()->id}}" style="display: none">
                                      <p class="text-center" style="margin-left: 10%">Verify your password to proceed to editing profile</p>
                                    </div>        
                          </div>
                          <div class="modal-footer">
                          <input type="submit" class="btn btn-outline-primary btn-lg" value="Verify">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            @yield('content')
        </main>


        <footer class="footer footer-static footer-light navbar-border">
      Developed by <i class="fa fa-love"></i><a href="https://www.tizycorp.com" target="_blank" >Tizycorp Digital</a>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{asset('robust/app-assets/js/core/libraries/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/ui/tether.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/js/core/libraries/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/ui/unison.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/ui/blockUI.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/ui/jquery.matchHeight-min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/ui/screenfull.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/vendors/js/extensions/pace.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{asset('robust/app-assets/vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="{{asset('robust/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
    <script src="{{asset('robust/app-assets/js/core/app.js') }}" type="text/javascript"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('robust/app-assets/js/scripts/pages/dashboard-lite.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <script type="text/javascript">
      function close(){
        document.getElementById('success_btn').style.display = "none";
      }
    </script>

    <!-- data table -->
     <script src="{{asset('references/js/data-table/datatables.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/jszip.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/pdfmake.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/vfs_fonts.js')}}"></script>
    <script src="{{asset('references/js/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/buttons.print.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('references/js/data-table/datatables-init.js')}}"></script>

  </body>
</html>