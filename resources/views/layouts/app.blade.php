<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('references/js/jquery.js') }}"></script>
    <script src="{{asset('references/js/popper.min.js') }}"></script>
    <script src="{{asset('references/js/bootstrap.min.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{asset('references/css/toastr.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('references/css/bootstrap.min.css')}}">
    <link href="{{ asset('references/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('references/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('references/css/responsive.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('references/images/tizy.png')}}"   type="image/x-icon">
    <link rel="icon" href="{{asset('references/images/tizy.png')}}" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>


    <script src="{{asset('references/js/appear.js') }}"></script>
    <script src="{{asset('references/js/lazyload.js')}}"></script>
    <script src="{{ asset('references/js/jquery.fancybox.js')}}"></script>
    <script src="{{ asset('references/js/jquery-ui.js')}}"></script>
    <script src="{{ asset('references/js/wow.js')}}"></script>
    <script src="{{ asset('references/js/owl.js')}}"></script>
    <script src="{{ asset('references/js/paroller.js')}}"></script>
    <script src="{{ asset('references/js/respond.js')}}"></script>
    <script src="{{ asset('references/js/script.js')}}"></script>
    <script src="{{ asset('references/js/toastr.min.js')}}"></script>
    <script src="{{ asset('references/js/toastr.init.js')}}"></script>

</body>
</html>
