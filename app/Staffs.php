<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Staffs extends Model
{

        use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
        'user_id','first_name','last_name', 'email', 'phone', 'gender','job_spec',
    ];  

     protected $hidden = [
        'password',
    ];

    public function user()
    {

// return $this->belongsTo(User::class);

    }
}
