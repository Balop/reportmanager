<?php

namespace App\Http\Controllers;
use App\User;
use App\Staffs;
use App\Report;
use Auth;
use DB;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $report = DB::table('reports')
        ->join('staffs','staffs.id', '=', 'reports.user_id')
        ->select('reports.*','staffs.first_name','staffs.last_name','staffs.job_spec') 
        ->get();

        //this will only select the first 3 rows in the db

        $employeecount = DB::table('users')
        ->select('users.*')->count();


       $person = DB::table('reports')
              ->join('staffs','staffs.id', '=', 'reports.user_id')
              ->select('reports.*','staffs.first_name','staffs.last_name','staffs.job_spec')
              ->where('reports.user_id', '=', Auth::user()->id) //this will only select the first 3 rows in the db
              ->get();

        return view('home', ['reports' => $report, 'persons' => $person, 'employeecount' => $employeecount]);
        
    }

     public function new_employee()
    {

       $job = DB::table('jobs')
              ->select('jobs.*')
              ->get();

      return view('new_client', ['jobs' => $job]);
         
    }


    public function checkedit(Request $request)
    {

      $obj = User::findorfail($request->id);

          $data = Hash::check($request->password, $obj->password);

          
       if($data){

        



        return redirect()->route('edit_user');

       }

       else{

       return redirect()->back()->with('success', 'The password supplied is incorrect');
          
          }

      }


      public function edit_user()
    {

       $user = User::findorfail(Auth::user()->id);
        $profile = Staffs::findorfail(Auth::user()->id); 


        return view('edit_user', ['user' => $user, 'profile' => $profile]);
        
    }



     public function report()
    {

       $job = DB::table('staffs')
              ->select('staffs.job_spec')
              ->where('staffs.id', '=', Auth::user()->id) 
              ->first();

      return view('report', ['job' => $job]);
         
    }


     public function reports()
    {

     $report = DB::table('reports')
              ->join('staffs','staffs.id', '=', 'reports.user_id')
              ->select('reports.*','staffs.first_name','staffs.last_name','staffs.job_spec')
              ->get();

        return view('view_reports', ['reports' => $report]);

    
    }

     public function save(Request $request){
        $this->validate($request, [
          'job_spec' => ['required', 'string', 'max:225'],
          'fname' => ['required', 'string', 'max:255'],
          'lname' => ['required', 'string', 'max:255'],
          'email' => ['required', 'email',  'max:225', 'unique:users', 'unique:staffs'],
          'phone' => ['required', 'string', 'min:11', 'max:15'],
          'username' => ['required', 'string', 'max:255', 'unique:users'],
          'password' => 'required|confirmed|min:8',
          'gender' => ['required', 'string', 'max:6']
      ]);
      

        $obj = new Staffs(); 

        $obj->job_spec = $request->job_spec;
        $obj->first_name = $request->fname;
        $obj->last_name = $request->lname;
        $obj->email = $request->email;
        $obj->phone = $request->phone;
        $obj->gender = $request->gender; 


        $obj->save();


        $object = new User(); 

        $object->username = $request->username;
        $object->password = Hash::make($request['password']);
        $object->email = $request->email;

        $object->save();



         return redirect()->back()->with('success', 'Employee Information saved successfully');   


        // }

}


        public function editdetails(Request $request){
                $this->validate($request, [
                  'fname' => ['required', 'string', 'max:255'],
                  'lname' => ['required', 'string', 'max:255'],
                  'email' => ['required', 'email',  'max:225'],
                  'phone' => ['required', 'string', 'min:11', 'max:15'],
                  'username' => ['required', 'string', 'max:255'],
                  'password' => 'required|confirmed|min:8',
              ]);
      

                $obj = Staffs::findorfail(Auth::user()->id); 

                
                $obj->first_name = $request->fname;
                $obj->last_name = $request->lname;
                $obj->email = $request->email;
                $obj->phone = $request->phone;

                $obj->save();


        $object = User::findorfail(Auth::user()->id); 

        $object->username = $request->username;
        $object->password = Hash::make($request['password']);
        $object->email = $request->email;

        $object->save();



         return redirect()->back()->with('success', 'Proile updated successfully');   


}




 public function save_report(Request $request){

        $this->validate($request, [
          'job' => ['required', 'string', 'max:225'],
          'progress' => ['required', 'string', 'max:255'],
          'description' => ['required', 'string', 'max:255'],
          'challenge' => ['max:255'],
      ]);
        

        $obj = new Report(); 

        $obj->user_id = Auth::user()->id;
        $obj->work_accomplished = $request->job;
        $obj->progress = $request->progress;
        $obj->description = $request->description;
        if($request->challenge != ""){
        $obj->challenge = $request->challenge;
       }
        $obj->save();

         return redirect()->back()->with('success', 'Your report has been submitted successfully');   

}



    public function deleteclient(Request $request){

    $data = Client::findOrFail($request->id); 
    $data->delete();  

         return redirect()->back()->with('deleted', 'Client information deleted successfully');   
      }  


       public function updateclient(Request $request)
     {
        
        $update = Client::findOrFail($request->id); 
        $update->first_name = $request->fname;
        $update->last_name = $request->lname;
        $update->email = $request->mail;
        $update->website_url = $request->website_url;
        $update->address = $request->address;
        $update->postal_code = $request->postal_code;
        $update->state = $request->state; 
        $update->city = $request->city;
        $update->country = $request->country;
        $update->phone = $request->phone;
        $update->facebook = $request->facebook;
        $update->instagram = $request->instagram;
        $update->twitter = $request->twitter;


        $update->save();


             return redirect()->back()->with('success', 'Client Information successfully updated');
             
      
     }

}