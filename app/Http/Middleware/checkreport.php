<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;


class checkreport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_role != 1){

             return redirect()->route('home')->with('success', 'You are not allowed to visit that page');
          }
        return $next($request);
    }
}
