<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Report extends Model
{

        use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
        'user_id','work_accomplished','description','challenge','progress',]; 

     protected $hidden = [
        'created_at',
    ];

    public function user()
    {

// return $this->belongsTo(User::class);

    }
}
